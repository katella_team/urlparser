package com.katella.urlparser;

import android.content.Context;

import com.katella.urlparser.data.CacheService;
import com.katella.urlparser.network.NetworkService;
import com.katella.urlparser.parsing.HtmlParser;
import com.katella.urlparser.parsing.URLValidator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Eugene Katella on 07.12.2015.
 *
 * <p>Unit tests for MainPresenter implementation</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class MainPresenterImplTest {

    private String testUrl = "http://www.example.com";
    private final String testResponse = "<html></html>";
    private final List<String> testResult = Arrays.asList("http://url1.com", "http://url2.com", "http://url3.com");

    @Mock
    private MainView mainView;

    @Mock
    private NetworkService networkService;

    @Mock
    private CacheService cacheService;

    @Mock
    private HtmlParser htmlParser;

    @Mock
    private Context context;

    private MainPresenterImpl presenter;



    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new MainPresenterImpl(mainView, context, networkService, htmlParser, cacheService);
    }

    @Test
    public void testMainPresenterImpl_loadUrl_success() {
        // Arrange
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                presenter.onLoadSuccess(testResponse);
                return null;
            }
        }).when(networkService).loadFromURL(testUrl, presenter);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                presenter.onParseSuccess(testResult);
                return null;
            }
        }).when(htmlParser).parseHtml(testResponse, presenter);

        // Act
        presenter.loadUrl(testUrl);

        // Assert
        Mockito.verify(mainView).setButtonEnabled(false);
        Mockito.verify(mainView).setInputEnabled(false);
        Mockito.verify(mainView).showProgress();
        Mockito.verify(networkService).loadFromURL(testUrl, presenter);
        Mockito.verify(htmlParser).parseHtml(testResponse, presenter);
        Mockito.verify(cacheService).putResultsList(testResult);
        Mockito.verify(mainView).setItems(testResult);
        Mockito.verify(mainView).setButtonEnabled(true);
        Mockito.verify(mainView).setInputEnabled(true);
        Mockito.verify(mainView).hideProgress();
    }

    @Test
    public void testMainPresenterImpl_loadUrl_failWithNetworkError() {
        // Arrange
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                presenter.onLoadError(new UnknownHostException());
                return null;
            }
        }).when(networkService).loadFromURL(testUrl, presenter);

        String message = "Test error message";
        Mockito.when(context.getString(R.string.unknown_host_error)).thenReturn(message);


        // Act
        presenter.loadUrl(testUrl);

        // Assert
        Mockito.verify(mainView).setButtonEnabled(false);
        Mockito.verify(mainView).setInputEnabled(false);
        Mockito.verify(mainView).showProgress();
        Mockito.verify(networkService).loadFromURL(testUrl, presenter);
        Mockito.verify(context).getString(R.string.unknown_host_error);
        Mockito.verify(mainView).showMessage(message);
        Mockito.verify(mainView).setButtonEnabled(true);
        Mockito.verify(mainView).setInputEnabled(true);
        Mockito.verify(mainView).hideProgress();
    }

    @Test
    public void testMainPresenterImpl_loadUrl_failWithParsingError() {
        // Arrange
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                presenter.onLoadSuccess(testResponse);
                return null;
            }
        }).when(networkService).loadFromURL(testUrl, presenter);

        final String testMessage = "Test message";
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {

                presenter.onParseError(new RuntimeException(testMessage));
                return null;
            }
        }).when(htmlParser).parseHtml(testResponse, presenter);

        // Act
        presenter.loadUrl(testUrl);

        // Assert
        Mockito.verify(mainView).setButtonEnabled(false);
        Mockito.verify(mainView).setInputEnabled(false);
        Mockito.verify(mainView).showProgress();
        Mockito.verify(networkService).loadFromURL(testUrl, presenter);
        Mockito.verify(htmlParser).parseHtml(testResponse, presenter);
        Mockito.verify(mainView).showMessage(testMessage);
        Mockito.verify(mainView).setButtonEnabled(true);
        Mockito.verify(mainView).setInputEnabled(true);
        Mockito.verify(mainView).hideProgress();
    }

    @Test
    public void testMainPresenterImpl_loadUrl_null() {
        // Act
        presenter.loadUrl(null);

        // Assert
        Mockito.verifyZeroInteractions(mainView, networkService, cacheService, context);
    }

    @Test
    public void testMainPresenterImpl_onItemClicked() {
        // Arrange
        String testItem = "item";
        // Act
        presenter.onItemClicked(testItem);
        // Assert
        Mockito.verify(mainView).setUrlText(testItem);
    }

    @Test
    public void testMainPresenterImpl_onInputChanged_whenActive() {
        // Arrange
        String testInput = "http://www.example.com";
        boolean isValid = URLValidator.isURLValid(testInput);
        // Act
        presenter.onInputChanged(testInput);
        // Assert
        Mockito.verify(cacheService).putUrl(testInput);
        Mockito.verify(mainView).setButtonEnabled(isValid);
    }

    @Test
    public void testMainPresenterImpl_onInputChanged_whenProcessing() {
        // Arrange
        String testInput = "http://www.example.com";
        presenter.loadUrl(testInput);
        // Act
        presenter.onInputChanged(testInput);
        // Assert
        Mockito.verify(cacheService).putUrl(testInput);
        Mockito.verify(mainView).setButtonEnabled(false);
    }

    @Test
    public void testMainPresenterImpl_restoreState() {
        // Arrange
        String testUrl = "testUrl";
        List<String> testResultList = Arrays.asList("url1", "url2");
        Mockito.when(cacheService.getUrl()).thenReturn(testUrl);
        Mockito.when(cacheService.getResultsList()).thenReturn(testResultList);
        // Act
        presenter.restoreState();
        // Assert
        Mockito.verify(cacheService).getUrl();
        Mockito.verify(mainView).setUrlText(testUrl);
        Mockito.verify(cacheService).getResultsList();
        Mockito.verify(mainView).setItems(testResultList);
    }

    @Test
    public void testMainPresenterImpl_restoreState_noData() {
        // Arrange
        Mockito.when(cacheService.getUrl()).thenReturn(null);
        Mockito.when(cacheService.getResultsList()).thenReturn(new ArrayList<String>());
        // Act
        presenter.restoreState();
        // Assert
        Mockito.verify(cacheService).getUrl();
        Mockito.verify(cacheService).getResultsList();
        Mockito.verifyZeroInteractions(mainView, cacheService);
    }
}