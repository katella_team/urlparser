package com.katella.urlparser;

import android.content.Context;
import android.content.SharedPreferences;
import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import junit.framework.Assert;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Eugene Katella on 08.12.2015.
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Button loadButton;
    private EditText urlView;
    private ListView listView;
    private ProgressBar progressBar;

    private MainActivity activity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        loadButton = (Button) activity.findViewById(R.id.get_links_button);
        urlView = (EditText) activity.findViewById(R.id.url_field);
        listView = (ListView) activity.findViewById(android.R.id.list);
        progressBar = (ProgressBar) activity.findViewById(android.R.id.progress);
    }

    public void tearDown() throws Exception {
        SharedPreferences.Editor editor = activity.getSharedPreferences("com.katella.urlparser.preferences", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        activity.finish();
    }

    @MediumTest
    @UiThreadTest
    public void testActivity_setItems() {
        // Arrange
        List<String> strings = Arrays.asList("testString1", "testString2");
        // Act
        activity.setItems(strings);

        // Assert
        Assert.assertEquals(2, listView.getCount());
        Assert.assertEquals(strings.get(0), listView.getItemAtPosition(0));
        Assert.assertEquals(strings.get(1), listView.getItemAtPosition(1));
    }

    @MediumTest
    @UiThreadTest
    public void testActivity_setUrlViewText() {
        // Arrange
        String testString = "testString";
        // Act
        activity.setUrlText(testString);
        // Assert
        Assert.assertEquals(testString,urlView.getText().toString());
    }

    @MediumTest
    @UiThreadTest
    public void testActivity_setButtonEnabled() {
        // Arrange
        boolean current = loadButton.isEnabled();
        // Act
        activity.setButtonEnabled(!current);
        // Assert
        Assert.assertEquals(!current, loadButton.isEnabled());
    }


    @MediumTest
    @UiThreadTest
    public void testActivity_setInputEnabled() {
        // Arrange
        boolean current = urlView.isEnabled();
        // Act
        activity.setInputEnabled(!current);
        // Assert
        Assert.assertEquals(!current, urlView.isEnabled());

    }

    @MediumTest
    public void testActivityIsCorrectlyInitializedOnStartup() {
        // Assert
        Assert.assertFalse(loadButton.isEnabled());
        Assert.assertTrue(urlView.isEnabled());
        Assert.assertEquals("", urlView.getText().toString());
        Assert.assertEquals(View.INVISIBLE, progressBar.getVisibility());
        Assert.assertEquals(0, listView.getCount());
    }

    @MediumTest
    @UiThreadTest
    public void testActivityEnablesButtonOnValidInput() {
        // Arrange
        Assert.assertFalse(loadButton.isEnabled());
        Assert.assertEquals("", urlView.getText().toString());
        String testUrl = "http://www.example.com";
        // Act
        urlView.setText(testUrl);
        // Assert
        Assert.assertTrue(loadButton.isEnabled());
        Assert.assertEquals(testUrl, urlView.getText().toString());
    }

    @MediumTest
    @UiThreadTest
    public void testActivityNotEnablesButtonOnInValidInput() {
        // Arrange
        Assert.assertFalse(loadButton.isEnabled());
        Assert.assertEquals("", urlView.getText().toString());
        String testUrl = "http:*(&%//www.example.com";
        // Act
        urlView.setText(testUrl);
        // Assert
        Assert.assertFalse(loadButton.isEnabled());
        Assert.assertEquals(testUrl, urlView.getText().toString());
    }



    @LargeTest
    @UiThreadTest
    public void testActivityChangesStateOnButtonTouch() {
        // Arrange
        String testUrl = "http://example.com";
        // Act
        urlView.setText(testUrl);
        loadButton.performClick();
        // Assert
        Assert.assertFalse(loadButton.isEnabled());
        Assert.assertFalse(urlView.isEnabled());
        Assert.assertTrue(progressBar.isEnabled());
    }


}