package com.katella.urlparser.network;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Network service interface. Declares functionality for asynchronous downloading contents, placed at
 * passed URL. Only content with type "html/text" should be downloaded. For other content types an appropriate exception
 * must be thrown. Loading results are returned in a respective delegate method of {@link com.katella.urlparser.network.LoadResultListener}.</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface NetworkService {

    /**
     * Loads URL content asynchronously and returns results to a listener
     * @param url URL string to load
     * @param listener listener, which handles results of loading
     */
    void loadFromURL(String url, LoadResultListener<String> listener);

    /**
     * Cancels downloading process, if it is running. Otherwise does nothing
     */
    void cancel();
}
