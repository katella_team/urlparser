package com.katella.urlparser.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.katella.urlparser.BaseAsyncTask;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>{@link com.katella.urlparser.network.NetworkService} implementation</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class NetworkServiceImpl implements NetworkService {

    private WeakReference<Context> context;

    public NetworkServiceImpl(Context context) {
        this.context = new WeakReference<>(context);
    }


    private BaseAsyncTask<String, Void, String> task;

    @Override
    public void loadFromURL(String url, final LoadResultListener<String> listener) {
            task = new BaseAsyncTask<String, Void, String>() {
                @Override
                protected String doWork(String... params) throws Exception {
                    if (! isConnected()) {
                        throw new NetworkIsNotAvailableException();
                    }
                    String url = params[0];
                    return downloadFromURL(url);
                }

                @Override
                protected void onSuccess(String result) {
                    listener.onLoadSuccess(result);
                }

                @Override
                protected void onError(Exception exception) {
                    listener.onLoadError(exception);
                }
            };
        task.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, url);
    }

    @Override
    public void cancel() {
        if (task != null && ! task.getStatus().equals(AsyncTask.Status.FINISHED)) {
            task.cancel(true);
        }
    }

    /**
     * Downloads passed URL contents and returns it as a string.
     * <p>Works synchronously, so should be called in a non-UI thread</p>
     * @param urlString URL string to download from
     * @return downloaded content
     * @throws Exception
     */
    private String downloadFromURL(String urlString) throws Exception{

        URL url = new URL(urlString);
        URLConnection urlConnection = url.openConnection();
        urlConnection.connect();

        String contentType = urlConnection.getContentType();
        if (contentType!= null && ! contentType.contains("text/html")) {
            throw new WrongContentTypeException(contentType);
        }
        int contentLength = urlConnection.getContentLength();
        if (contentLength < 0) {
            contentLength = 1024;
        }
        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
        OutputStream out = new ByteArrayOutputStream(contentLength);

        byte[] buffer = new byte[1024];
        int count;
        while ((count = in.read(buffer)) != -1) {
            out.write(buffer, 0, count);
        }

        String result = out.toString();
        out.flush();

        out.close();
        in.close();
        return result;
    }

    /**
     * Checks network availability
     * @return true if there is an accessible network, false otherwise
     */
    private boolean isConnected() {
        ConnectivityManager cm =
                (ConnectivityManager)context.get().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }
}

