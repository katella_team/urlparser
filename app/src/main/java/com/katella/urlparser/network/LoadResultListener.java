package com.katella.urlparser.network;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Loading results listener. Declares functionality for handling loading results.</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface LoadResultListener<T> {

    /**
     * Called, when loading is successfully finished
     * @param result loading results
     */
    void onLoadSuccess(T result);

    /**
     * Called, when loading failed
     * @param error exception, which caused loading failure
     */
    void onLoadError(Exception error);
}
