package com.katella.urlparser.network;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Exception class. Used to determine situations, when user tries to download content with non-appropriate type</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class WrongContentTypeException extends RuntimeException {
    private String passedContentType;

    public String getPassedContentType() {
        return passedContentType;
    }

    public WrongContentTypeException(String passedContentType) {
        this.passedContentType = passedContentType;
    }
}
