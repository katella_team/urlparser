package com.katella.urlparser.network;

/**
 * Created by Eugene Katella on 02.12.2015.
 *
 * <p>Exception class, needed to determine situations, when user tried to load data from network, while it's not available</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class NetworkIsNotAvailableException extends RuntimeException {
}
