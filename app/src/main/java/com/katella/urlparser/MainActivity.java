package com.katella.urlparser;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.katella.urlparser.data.SharedPrefsCacheServiceImpl;
import com.katella.urlparser.network.NetworkServiceImpl;
import com.katella.urlparser.parsing.HtmlRegexParserImpl;

import java.util.List;

/**
 * Main activity of the application
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements MainView, AdapterView.OnItemClickListener, TextWatcher, View.OnClickListener {

    private Button loadButton;
    private EditText urlView;
    private ListView listView;
    private ProgressBar progressBar;

    private MainPresenter presenter;

    public void setPresenter(MainPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init views
        this.loadButton = (Button) findViewById(R.id.get_links_button);
        this.urlView = (EditText) findViewById(R.id.url_field);
        this.listView = (ListView) findViewById(android.R.id.list);
        this.progressBar = (ProgressBar) findViewById(android.R.id.progress);

        this.listView.setOnItemClickListener(this);
        this.loadButton.setOnClickListener(this);
        this.urlView.addTextChangedListener(this);

        this.urlView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                loadButton.performClick();
                return false;
            }
        });

        this.setPresenter(new MainPresenterImpl(this,
                this,
                new NetworkServiceImpl(this),
                new HtmlRegexParserImpl(),
                new SharedPrefsCacheServiceImpl(this)));

        this.listView.setAdapter(new ArrayAdapter<String>(this, R.layout.list_item, R.id.url_list_item));

        this.presenter.restoreState();
    }

    @Override
    protected void onDestroy() {
        this.presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void setItems(List<String> items) {
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) this.listView.getAdapter();
        adapter.clear();
        adapter.addAll(items);
        if (items.size() > 0) {
            listView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void setUrlText(String url) {
        if (url == null) {
            url = "";
        }
        this.urlView.setText(url);
    }

    @Override
    public void setButtonEnabled(boolean isEnabled) {
        this.loadButton.setEnabled(isEnabled);
    }

    @Override
    public void setInputEnabled(boolean isEnabled) {
        this.urlView.setEnabled(isEnabled);
    }

    @Override
    public void showProgress() {
        this.progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String item = ((String) this.listView.getAdapter().getItem(position));
        this.presenter.onItemClicked(item);
    }

    @Override
    public void afterTextChanged(Editable s) {
        this.presenter.onInputChanged(s.toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // nothing
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // nothing
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.get_links_button:
                InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);

                String url = this.urlView.getText().toString();
                this.presenter.loadUrl(url);
                break;
        }
    }


}
