package com.katella.urlparser.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Helper class for validating HTTP URLs</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public final class URLValidator {

    public static final Pattern pattern = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

    private URLValidator(){}

    /**
     * Checks if passed URL string is valid
     * @param url URL string
     * @return true if URL is valid, false otherwise
     */
    public static boolean isURLValid(String url) {
        Matcher matcher = pattern.matcher(url);
        return matcher.matches();
    }
}
