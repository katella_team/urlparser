package com.katella.urlparser.parsing;

import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;
import android.text.style.URLSpan;

import com.katella.urlparser.BaseAsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eugene Katella on 06.12.15.
 *
 * <p>{@link com.katella.urlparser.parsing.HtmlParser} implementation based on DOM-parsing.
 * Currently uses standard Android {@link android.text.Html#fromHtml(String)} method to get all
 * {@link android.text.style.URLSpan} objects. This approach showed poor parsing quality, so later should be replaced
 * by some better DOM-parsing libraries.
 * </p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class DomHtmlParserImpl implements HtmlParser {

    private BaseAsyncTask<String, Void, List<String>> task;

    @Override
    public void parseHtml(String html, final ParseResultListener<List<String>> listener) {
        task = new BaseAsyncTask<String, Void, List<String>>(){
            @Override
            protected List<String> doWork(String... params) throws Exception {
                String param = params[0];
                Spanned spanned = Html.fromHtml(param);
                URLSpan[] spans = spanned.getSpans(0, spanned.length(), URLSpan.class);
                List<String> result = new ArrayList<>(spans.length);
                for (URLSpan span : spans) {
                    result.add(span.getURL());
                }
                return result;
            }

            @Override
            protected void onSuccess(List<String> strings) {
                listener.onParseSuccess(strings);
            }

            @Override
            protected void onError(Exception exception) {
                listener.onParseError(exception);
            }
        };
        task.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, html);
    }

    @Override
    public void cancel() {
        if (task != null && ! task.getStatus().equals(AsyncTask.Status.FINISHED)) {
            task.cancel(true);
        }
    }
}
