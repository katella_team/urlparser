package com.katella.urlparser.parsing;

import android.os.AsyncTask;

import com.katella.urlparser.BaseAsyncTask;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>{@link com.katella.urlparser.parsing.HtmlParser} implementation, based on a regular expression.
 * Uses {@link com.katella.urlparser.parsing.URLValidator#pattern} to find valid URLs</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class HtmlRegexParserImpl implements HtmlParser {

    private BaseAsyncTask<String, Void, List<String>> task;

    @Override
    public void parseHtml(String html, final ParseResultListener<List<String>> listener) {

        task = new BaseAsyncTask<String, Void, List<String>>() {
            @Override
            protected List<String> doWork(String... params) throws Exception {
                List<String> result = new ArrayList<>();
                String page = params[0];
                Matcher matcher = URLValidator.pattern.matcher(page);
                while (matcher.find()) {
                    result.add(matcher.group(0));
                }
                return result;
            }

            @Override
            protected void onSuccess(List<String> strings) {
                listener.onParseSuccess(strings);
            }

            @Override
            protected void onError(Exception exception) {
                listener.onParseError(exception);
            }
        };
        task.executeOnExecutor(BaseAsyncTask.SERIAL_EXECUTOR, html);
    }

    @Override
    public void cancel() {
        if (task != null && ! task.getStatus().equals(AsyncTask.Status.FINISHED)) {
            task.cancel(true);
        }
    }
}
