package com.katella.urlparser.parsing;

import java.util.List;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Html Parser interface. Declares functionality for asynchronous parsing html page and returning
 * found URLs to a listener.</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface HtmlParser {

    /**
     * Parses html string asynchronously and passes results to a listener
     * @param html html page string
     * @param listener listener, which processes the parsing results
     */
    void parseHtml(String html, ParseResultListener<List<String>> listener);

    /**
     * Cancels parsing process if it is running. Otherwise does nothing
     */
    void cancel();
}
