package com.katella.urlparser.parsing;

/**
 * Created by Eugene Katella on 02.12.2015.
 *
 * <p>Listener which handles results of parsing</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface ParseResultListener<T> {

    /**
     * Called when parsing successfully finishes
     * @param result parsing results
     */
    void onParseSuccess(T result);

    /**
     * Called when parsing fails
     * @param error exception, which caused failing
     */
    void onParseError(Exception error);
}
