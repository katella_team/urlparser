package com.katella.urlparser;

import java.util.List;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Interface for declaring view-specific behavior</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface MainView {

    void setItems(List<String> items);

    void setUrlText(String url);

    void setButtonEnabled(boolean isEnabled);

    void setInputEnabled(boolean isEnabled);

    void showProgress();

    void hideProgress();

    void showMessage(String message);

}
