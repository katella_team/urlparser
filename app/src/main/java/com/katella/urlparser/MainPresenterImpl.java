package com.katella.urlparser;

import android.content.Context;

import com.katella.urlparser.data.CacheService;
import com.katella.urlparser.network.LoadResultListener;
import com.katella.urlparser.network.NetworkIsNotAvailableException;
import com.katella.urlparser.network.NetworkService;
import com.katella.urlparser.network.WrongContentTypeException;
import com.katella.urlparser.parsing.HtmlParser;
import com.katella.urlparser.parsing.ParseResultListener;
import com.katella.urlparser.parsing.URLValidator;

import java.lang.ref.WeakReference;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>{@link com.katella.urlparser.MainPresenter} implementation</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class MainPresenterImpl implements MainPresenter, LoadResultListener<String>, ParseResultListener<List<String>> {

    private WeakReference<MainView> mainView;
    private WeakReference<Context> context;
    private NetworkService networkService;
    private HtmlParser parser;
    private CacheService cacheService;

    private boolean isProcessing = false;

    public MainPresenterImpl(MainView mainView, Context context, NetworkService networkService, HtmlParser parser, CacheService cacheService) {
        this.context = new WeakReference<>(context);
        this.mainView = new WeakReference<>(mainView);
        this.networkService = networkService;
        this.parser = parser;
        this.cacheService = cacheService;
    }

    @Override
    public void onItemClicked(String item) {
        this.mainView.get().setUrlText(item);
    }

    @Override
    public void loadUrl(String url) {
        if (url != null) {
            this.changeState(true);
            this.networkService.loadFromURL(url, this);
        }
    }

    @Override
    public void onInputChanged(String input) {
        this.cacheService.putUrl(input);
        if (! this.isProcessing) {
            this.mainView.get().setButtonEnabled(URLValidator.isURLValid(input));
        }
    }

    @Override
    public void restoreState() {
        String url = this.cacheService.getUrl();
        if (url != null) {
            this.mainView.get().setUrlText(url);
        }

        List<String> resultsList = this.cacheService.getResultsList();
        if (resultsList != null && ! resultsList.isEmpty()) {
            this.mainView.get().setItems(resultsList);
        }
    }

    @Override
    public void onDestroy() {
        this.networkService.cancel();
        this.parser.cancel();
    }

    @Override
    public void onLoadSuccess(String result) {
        this.parser.parseHtml(result, this);
    }

    @Override
    public void onLoadError(Exception error) {
        String message;
        if (error instanceof WrongContentTypeException) {
            message = String.format(this.context.get().getString(R.string.wrong_content_type_error),
                    ((WrongContentTypeException) error).getPassedContentType());
        } else if (error instanceof NetworkIsNotAvailableException) {
            message = this.context.get().getString(R.string.network_unavailable_error);
        } else if(error instanceof UnknownHostException) {
            message = this.context.get().getString(R.string.unknown_host_error);
        } else {
            message = error.getMessage();
        }
        this.mainView.get().showMessage(message);
        changeState(false);
    }

    @Override
    public void onParseSuccess(List<String> result) {
        this.cacheService.putResultsList(result);
        this.mainView.get().setItems(result);
        changeState(false);
    }

    @Override
    public void onParseError(Exception error) {
        String message = error.getMessage();
        if (message == null) {
            message = this.context.get().getString(R.string.parsing_error);
        }
        this.mainView.get().showMessage(message);
        changeState(false);
    }

    /**
     * Performs complex state change of presented view.
     * @param isProcessing true, if presented view must display processing, false if this view must
     *                     display itself as ready for interaction
     */
    private void changeState(boolean isProcessing) {

        if (isProcessing) {
            this.mainView.get().showProgress();
            this.mainView.get().setButtonEnabled(false);
            this.mainView.get().setInputEnabled(false);
        } else {
            this.mainView.get().setButtonEnabled(true);
            this.mainView.get().setInputEnabled(true);
            this.mainView.get().hideProgress();
        }
        this.isProcessing = isProcessing;
    }

}
