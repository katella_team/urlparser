package com.katella.urlparser.data;

import java.util.List;

/**
 * Created by Eugene Katella on 05.12.15.
 *
 * <p>Interface for caching service. Declares functionality for saving parsing results at the device storage.</p>
 * <p>It's assumed, that size of cached data is not big enough to significantly slow down application
 * performance during access to the cache. That's why this interface awaits its implementations to work
 * synchronously. But if supposed cache size will grow enough to spoil user experience, this service
 * should be used in background thread.</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface CacheService {

    /**
     * Gets last requested URL
     * @return URL string
     */
    String getUrl();

    /**
     * Saves url as last requested
     * @param url URL string to save
     */
    void putUrl(String url);

    /**
     * Gets list of last parsed URLs
     * @return list of URL strings
     */
    List<String> getResultsList();

    /**
     * Saves list of URL strings for later restoring
     * @param results list of URL strings to save
     */
    void putResultsList(List<String> results);

}
