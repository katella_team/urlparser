package com.katella.urlparser.data;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Eugene Katella on 05.12.15.
 *
 * <p>{@link com.katella.urlparser.data.CacheService} implementation, based on Android {@link android.content.SharedPreferences}.</p>
 * <br/>
 * <p>It must be mentioned, that the list of parsed URL strings is stored at SharedPreferences as a serialized JSON string.
 * Using standard {@link android.content.SharedPreferences.Editor#putStringSet(String, java.util.Set)} may seem as more appropriate
 * alternative for this purposes, but its implementation internally uses HashSet, which doesn't guarantee correct ordering of saved list.
 * </p>
 * @author Eugene Katella
 * @version 1.0
 */
public class SharedPrefsCacheServiceImpl implements CacheService {

    private Context context;

    // costants for accessing preferences
    private final String PREFS_NAME = "com.katella.urlparser.preferences";
    private final String SELECTED_URL_PREF_NAME = "selected_url";
    private final String PARSED_URLS_PREF_NAME = "parsed_urls";

    public SharedPrefsCacheServiceImpl(Context context) {
        this.context = context;
    }

    @Override
    public String getUrl() {
        return this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(SELECTED_URL_PREF_NAME, null);
    }

    @Override
    public void putUrl(String url) {
        SharedPreferences.Editor editor = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(SELECTED_URL_PREF_NAME, url);
        editor.apply();
    }

    @Override
    public List<String> getResultsList() {
        String serializedStrings = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).getString(PARSED_URLS_PREF_NAME, null);
        if (serializedStrings == null) {
            return null;
        }
        try {
            JSONArray array = new JSONArray(serializedStrings);
            List<String> result = new ArrayList<>(array.length());
            for (int i = 0; i < array.length(); i++) {
                result.add(array.getString(i));
            }
            return result;
        } catch (Exception exception) {
            SharedPreferences.Editor editor = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
            editor.remove(PARSED_URLS_PREF_NAME);
            editor.apply();
            return null;
        }
    }

    @Override
    public void putResultsList(List<String> results) {
        JSONArray array = new JSONArray(results);
        SharedPreferences.Editor editor = this.context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();

        editor.putString(PARSED_URLS_PREF_NAME, array.toString());
        editor.apply();
    }
}
