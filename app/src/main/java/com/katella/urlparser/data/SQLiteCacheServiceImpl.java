package com.katella.urlparser.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eugene Katella on 05.12.15.
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class SQLiteCacheServiceImpl implements CacheService {

    private SQLiteOpenHelper helper;

    public SQLiteCacheServiceImpl(SQLiteOpenHelper helper) {
        this.helper = helper;
    }

    @Override
    public String getUrl() {
        String[] projection = {URLParserDBContract.SelectedURLEntry.COLUMN_NAME_URL};
        Cursor cursor = this.helper.getReadableDatabase().query(URLParserDBContract.SelectedURLEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null,
                "1");
        String result = null;
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }
        return result;
    }

    @Override
    public void putUrl(String url) {
        SQLiteDatabase db = this.helper.getWritableDatabase();
        db.delete(URLParserDBContract.SelectedURLEntry.TABLE_NAME, null, null);

        ContentValues values = new ContentValues(1);
        values.put(URLParserDBContract.SelectedURLEntry.COLUMN_NAME_URL, url);
        db.insert(URLParserDBContract.SelectedURLEntry.TABLE_NAME, null, values);
    }

    @Override
    public List<String> getResultsList() {
        String[] projection = {URLParserDBContract.ParsedURLEntry.COLUMN_NAME_URL};
        Cursor cursor = helper.getReadableDatabase().query(URLParserDBContract.ParsedURLEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                URLParserDBContract.ParsedURLEntry._ID + " ASC",
                null);
        List<String> results = new ArrayList<>(cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                results.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return results;

    }


    @Override
    public void putResultsList(final List<String> results) {

        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(URLParserDBContract.ParsedURLEntry.TABLE_NAME, null, null);

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues(1);
            for (String result : results) {
                values.put(URLParserDBContract.ParsedURLEntry.COLUMN_NAME_URL, result);
                db.insert(URLParserDBContract.ParsedURLEntry.TABLE_NAME, null, values);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
