package com.katella.urlparser.data;

import android.provider.BaseColumns;

/**
 * Created by Eugene Katella on 05.12.15.
 *
 * <p>Class, which declares contract for accessing SQLite database.</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public final class URLParserDBContract {

    public URLParserDBContract() {}

    public static abstract class ParsedURLEntry implements BaseColumns {

        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_URL = "url";
    }

    public static abstract class SelectedURLEntry implements BaseColumns {

        public static final String TABLE_NAME = "selected";
        public static final String COLUMN_NAME_URL = "url";
    }
}
