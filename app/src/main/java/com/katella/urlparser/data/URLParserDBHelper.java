package com.katella.urlparser.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Eugene Katella on 05.12.15.
 *
 * <p>Custom SQLiteOpenHelper implementation used for creating database at first access.</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public class URLParserDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "URLParser.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String SQL_CREATE_PARSED_ENTRIES =
            "CREATE TABLE " + URLParserDBContract.ParsedURLEntry.TABLE_NAME + " (" +
                    URLParserDBContract.ParsedURLEntry._ID + " INTEGER PRIMARY KEY," +
                    URLParserDBContract.ParsedURLEntry.COLUMN_NAME_URL + TEXT_TYPE +" )";
    private static final String SQL_CREATE_SELECTED_ENTRIES =
            "CREATE TABLE " + URLParserDBContract.SelectedURLEntry.TABLE_NAME + " (" +
                    URLParserDBContract.SelectedURLEntry._ID + " INTEGER PRIMARY KEY," +
                    URLParserDBContract.SelectedURLEntry.COLUMN_NAME_URL + TEXT_TYPE +" )";

    public URLParserDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_PARSED_ENTRIES);
        db.execSQL(SQL_CREATE_SELECTED_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // doNothing
    }
}
