package com.katella.urlparser;

import android.os.AsyncTask;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>{@link android.os.AsyncTask} implementation, which provides abstract methods for making useful work
 * and handling both successful and failed results. Suites cases, when errors, occurred during background work
 * should be delivered to UI</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public abstract class BaseAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private Exception exception = null;

    @Override
    protected Result doInBackground(Params... params) {
        try {
            return this.doWork(params);
        } catch (Exception exception) {
            this.exception = exception;
            exception.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        if (this.exception != null) {
            this.onError(this.exception);
        } else {
            this.onSuccess(result);
        }
    }

    /**
     * Makes all useful work
     * @param params task params
     * @return work results
     * @throws Exception
     */
    protected abstract Result doWork(Params... params) throws Exception;

    /**
     * Called, when main work is finished successfully
     * @param result main work results
     */
    protected abstract void onSuccess(Result result);

    /**
     * Called when main work fails with exception
     * @param exception occurred exception
     */
    protected abstract void onError(Exception exception);

}
