package com.katella.urlparser;

/**
 * Created by Eugene Katella on 01.12.2015.
 *
 * <p>Presenter interface for main activity</p>
 *
 * @author Eugene Katella
 * @version 1.0
 */
public interface MainPresenter {

    void onItemClicked(String item);

    void loadUrl(String url);

    void onInputChanged(String input);

    void restoreState();

    void onDestroy();

}
